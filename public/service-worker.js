// Listen for install event, set callback
self.addEventListener('install', function(event) {
    // Perform some task
    console.log("service worker installed",event);
});

self.addEventListener('activate', function(event) {
  // Perform some task
  console.log("service worker activated",event);
});

self.addEventListener("fetch", (event) => {
  // Let the browser do its default thing
  // for non-GET requests:
  if (event.request.method != "GET") return;
  // and others as well:
  return;

});