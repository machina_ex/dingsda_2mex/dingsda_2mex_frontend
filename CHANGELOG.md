# 1.4.3

## Changes 

- 


# 1.4.2

## Changes 

- autosearch after filters changed

# 1.4.1

## Changes

- add load more button to end of search pages that changes after 0 items were found
- optimize toast/pop ups after searches/pagination

## Fixes

- minor fixes around pagination and toasts


# 1.4.0

## Changes

adds pagination to search view
keeps scroll position in search view
refactoring mapping of form fields between add and edit
adds picture/image edit option (rotation and delete) for images uploaded to dingsda2mex server (not linked images)
several README and documentation changes


## Fixes

fixes issue with not working enter button on some mobile devices
fixes double tags displayed in search view