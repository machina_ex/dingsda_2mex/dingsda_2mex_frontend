
```mermaid

graph TD;
    A[INPUT]-->B;
    B[is lat/long]-->D[endpoint:<br>/items/_geosearch];
    D-->E[returns:<br>Items in this location<br>]
    E-->F[autocomplete offers user:<br>Items<br>AND<br>Button to create new Location for this item]
    
    A-->B1;
    B1[is itemID]
    B1-->C1[endpoint:<br>/items/<itemID>]
    C1-->D1[returns: one item]
    D1-->E1[no autocomplete.<br>fill out <br>+<br> show preview of item and map]

    A-->B2;
    B2[is locationID]
    B2-->C2[endpoint:<br>/places/<locationID>]
    C2-->D2[returns: one place]
    D2-->E2[no autocomplete.<br>fill out <br>+<br> show preview/map]

    A-->B3;
    B3[is String<br>and not lat,long]-->C3[endpoint:<br>/items/_searchByName]
    C3-->D3[returns:<br>Items with mathing name]
    D3-->E3[autocomplete offers user:<br>Items]

```