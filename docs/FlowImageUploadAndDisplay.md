

# AS IS
```mermaid
graph TD;

ImageUpload.vue--"Events:<br> imageUploadChangedPreviewImages <br>updates item.pictures"-->EditItem.vue

ImageUpload.vue--"Events:<br> imageUploadChangedPreviewImages <br>updates item.pictures"-->AddItem.vue

ImageUpload.vue--"imageArray<br>.photoEditModeOn"-->DingGallery.vue

DingGallery.vue--"Events:<br> imageswipes<br>imagetouchs"-->ImageUpload.vue

ImageUpload.vue---->Actions.setPhotosToUpload--sets-->state.data.photosToUpload

ImageUpload.vue---->Actions.removePhotosFromUploadList--empties-->state.data.photosToUpload

AddItem.vue--item.pictures-->Actions.addItem

EditItem.vue--item.pictures-->Actions.editItem

Actions.editItem ----> Actions.uploadPhotosFromStoreAndGetUrls

Actions.addItem ----> Actions.uploadPhotosFromStoreAndGetUrls

Actions.uploadPhotosFromStoreAndGetUrls --gets--> state.data.photosToUpload --> Actions.uploadPhotosFromStoreAndGetUrls--empties<br>after upload-->state.data.photosToUpload

Actions.uploadPhotosFromStoreAndGetUrls--POST as FormData-->/uploads
```

## TO add rotation of pictures:

- [ ] rotate preview picture
- [ ] add remove original preview picture from 
    - [ ] item.pictures (if already uploaded)
    - [ ] state.data.photosToUpload (if not uploaded yet.) (OPTIONAL)
- [ ] edit item / add item and therefore upload new picture

## Further todo: 
- [ ] server function to find all userimages not linked to any item

# AS SHOULD

- [x] all actions as extra buttons left of EDIT/END EDITING button
- [x] mark picture in focus with pink border (focus by click)
- [ ] transform every file always into DataUrl() and save them as those in the state.