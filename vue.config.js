// vue.config.js

process.env.VUE_APP_VERSION = require('./package.json').version
/**
 * @type {import('@vue/cli-service').ProjectOptions}
 */
 module.exports = {
    // options...
    publicPath: "./",
     configureWebpack: {
      plugins: [],
      module: { 
        rules: [{
          test: /\.mjs$/, // workaround to make vueuse usable with vue-cli
          include: /node_modules/,
          type: "javascript/auto"
        }]
      }
    },
    devServer: {
      proxy: 'http://localhost:8080'
    }
  }
  