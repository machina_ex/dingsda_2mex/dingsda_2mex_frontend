# dingsda 2mex webclient/ frontend

this repository is part of the MACHINA COMMONS project <a href="https://gitlab.com/machina_ex/dingsda_2mex">dingsda 2mex</a>. 

This repository contains the frontend/webclient/user interface to add, edit, manage items in your dingsda 2mex storage database. For the backend/server/api/database part please checkout the <a href="https://gitlab.com/machina_ex/dingsda_2mex/dingsda_2mex_backend">the backend repository</a>

## user documentation
user documentation about how to use the app after install can be found at https://machina_ex.gitlab.io/dingsda_2mex/dingsda2mex_tutorials/

## Project setup
1. install dependencies
```
npm install
```
2. create `.env.development` and `.env.production` from the example files and replace the value of VUE_APP_API_URL to your instance of the <a href="https://gitlab.com/machina_ex/dingsda_2mex/dingsda_2mex_backend">the backend server's</a>
 `/api` endpoint. (otherwise your client will try to talk to our server). Don't forget to add the `.` before the files.
3. add your own mapTiler and VUE_APP_MAPBOX_ACCESSTOKEN keys (this project uses maptiler.com inside `/src/components/Map.vue` for tiles and mapbox.com inside `/src/store/actions.js` for geocoding but feel free to adapt/add other 3rd party map tiling provider apis) to the .env.development and .env.production files
4. If you want to allow users to directly add tags, make sure that your server also allows that and then add (optionally) `VUE_APP_DIRECT_TAG_ADDING_ALLOWED=true` to the .env files. Learn more about this option in [the dingsda documentation](https://machina_ex.gitlab.io/tags.md)
```
    VUE_APP_MAPTILER_KEY=<your maptiler.com key>
    VUE_APP_MAPBOX_ACCESSTOKEN=<your mapbox.com key>
    VUE_APP_DIRECT_TAG_ADDING_ALLOWED=false
```
### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

#### config routes on webserver

since v1.4.0 dingsda_2mex_frontend requires you to configure your webservers fallback routes to index.html in order for this webapp to work. 

this is because we changed the vue internal routing history mode to <a href="https://router.vuejs.org/guide/essentials/history-mode.html#html5-mode">HTML5 mode</a>

Examples and explanations for the webserver configuration necessary can be found in <a href="https://router.vuejs.org/guide/essentials/history-mode.html#example-server-configurations">vue routers documentation page</a>

## Contributing
We are very much open to contributions. 

Feel free to submit issues or merge requests with changes you think make sense.

## License

Source code licensed MIT

## Authors and acknowledgment

This is a machina commons project by machina eX.  

gefördert durch Senatsverwaltung für Kultur und Europa Berlin  
<img src="https://machinacommons.world/img/Logo_Senat_Berlin.png" alt="Logo Senatsverwaltung für Kultur und Europa Berlin" width="400"/>
